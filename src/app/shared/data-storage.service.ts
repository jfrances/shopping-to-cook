
import { Http } from "@angular/http";
import { Injectable } from "@angular/core";
import { RecipeService } from "../services/recipe.service";
import { AuthService } from "../auth/auth.service";

@Injectable()
export class DataStorageService {
    url = 'https://ng-recipe-book-4dc97.firebaseio.com/';
    urlRecipes = this.url + 'recipes.json';

    constructor(
        private http: Http, 
        private recipeService: RecipeService,
        private authService: AuthService) {}

    storeRecipes(){
        const token = this.authService.getToken();
        const recipes = this.recipeService.getRecipes();
        return this.http.put(this.urlRecipes + '?auth=' + token, recipes);
    }

    fetchRecipes() {
        const token = this.authService.getToken();
        return this.http.get(this.urlRecipes + '?auth=' + token);
    }
}