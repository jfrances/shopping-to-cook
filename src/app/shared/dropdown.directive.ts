import { Directive, HostListener, Renderer, ElementRef, HostBinding } from "@angular/core";

@Directive({
    selector:'[appDropdown]'
})
export class DropdownDirective{
    @HostBinding('class.open') isOpen = false;

    constructor(
        private renderer: Renderer,
        private elementRef: ElementRef) { }

    @HostListener('click') 
    mouseover (eventData: Event) {
        this.isOpen = !this.isOpen;
    }
}