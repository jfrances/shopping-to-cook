import { Routes, RouterModule } from "@angular/router";
import { RecipesComponent } from "./recipes.component";
import { SelectRecipeComponent } from "./select-recipe/select-recipe.component";
import { RecipeEditComponent } from "./recipe-edit/recipe-edit.component";
import { RecipeDetailComponent } from "./recipe-detail/recipe-detail.component";
import { AuthGuard } from "../auth/auth-guard.service";
import { NgModule } from "@angular/core";
import { AuthService } from "../auth/auth.service";

const recipesRoutes: Routes = [{
    path: '', component: RecipesComponent, children: [
        {path: '', component: SelectRecipeComponent },
        {path: 'new', component: RecipeEditComponent, canActivate: [AuthGuard] },
        {path: ':id', component: RecipeDetailComponent },
        {path: ':id/edit', component: RecipeEditComponent, canActivate: [AuthGuard] }
    ]
}];

@NgModule({
    imports: [
        RouterModule.forChild(recipesRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
        AuthService
    ]
})
export class RecipesRoutingModule{}