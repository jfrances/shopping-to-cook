import { Ingredient } from "../shared/ingredient.model";

export class Recipe{
  public id: number;
  public name: string;
  public description: string;
  public imagePath: string;
  public ingredients: Ingredient[] = [];

  constructor(name: string, desc: string, imagePath: string){
    this.name = name;
    this.description = desc;
    this.imagePath = imagePath;
    this.ingredients = [
      {name: 'lettuce', amount: Math.floor(Math.random() * 9) + 1 },
      {name: 'tomato', amount: Math.floor(Math.random() * 9) + 1 },
      {name: 'pepper', amount: Math.floor(Math.random() * 9) + 1 }
    ];
  }
}