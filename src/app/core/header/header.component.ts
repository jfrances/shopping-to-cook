import { Component } from '@angular/core';
import { DataStorageService } from '../../shared/data-storage.service';
import { RecipeService } from '../../services/recipe.service';
import { Recipe } from '../../recipes/recipe.model';
import { map } from "rxjs/operators";
import { AuthService } from '../../auth/auth.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent{
    constructor(
        private storage: DataStorageService,
        private recipeService: RecipeService,
        private authService: AuthService){}

    storeRecipes() {
        this.storage.storeRecipes().subscribe(
            (response) => {
                console.log(response)
            });
    }

    fetchRecipes() {
        this.storage.fetchRecipes().pipe(map(
            (response) => {
                const recipes: Recipe[] = response.json();
                for(let rec of recipes){
                    if(!rec['ingredients']){
                        console.log(rec);
                        rec['ingredients'] = [];
                    }
                }
                return recipes;
            })).subscribe(
            (recipes: Recipe[]) => {
                this.recipeService.setRecipes(recipes);
            });
    }

    onLogout(){
        this.authService.logoutUser();
    }

    isAuthenticated() {
        return this.authService.isAuthenticated()
    }
}