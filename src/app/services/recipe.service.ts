import { Recipe } from "../recipes/recipe.model";
import { Subject } from "rxjs";

export class RecipeService {
    recipesChanged = new Subject<Recipe[]>();

    private recipes: Recipe[] = [
        new Recipe('A test recipe','This is a test','https://www.inspiredtaste.net/wp-content/uploads/2011/12/Pan-Roasted-Chicken-Bread-Recipe-3-1200.jpg'),
        new Recipe('Another test recipe','This is another','https://images.media-allrecipes.com/userphotos/250x250/131942.jpg'),
        new Recipe('The last test recipe','This is the last','https://www.bbcgoodfood.com/sites/default/files/styles/recipe/public/recipe/recipe-image/2016/09/frying-pan-pizza.jpg?itok=ELbVlLN7')
    ];

    getRecipes(){
        return this.recipes.slice();
    }

    getRecipeByIndex(index: number){
        return this.recipes[index];
    }

    getRecipe(id: number){
        return this.recipes.find(rec => rec.id === id);
    }

    addRecipe(recipe: Recipe){
        this.recipes.push(recipe);
        this.recipesChanged.next(this.recipes.slice());
    }

    updateRecipe(index: number, newRecipe: Recipe) {
        this.recipes[index] = newRecipe;
        this.recipesChanged.next(this.recipes.slice());
    }

    deleteRecipe(index: number){
        this.recipes.splice(index,1);
        this.recipesChanged.next(this.recipes.slice());
    }

    setRecipes(recipes: Recipe[]){
        this.recipes = recipes;
        this.recipesChanged.next(this.recipes.slice());
    }
}