import { Ingredient } from "../shared/ingredient.model";
import { Subject } from "rxjs";

export class ShoppingListService {
    private ingredients: Ingredient[] = [];

    onAddedIngredients = new Subject<Ingredient[]>();
    editingStarted = new Subject<number>();

    getIngredients(){
        return this.ingredients;
    }

    addIngredients(ingredients: Ingredient[]){
        this.ingredients.push(...ingredients);
        this.onAddedIngredients.next(ingredients);
    }

    getIngredient(index:number){
        return this.ingredients[index];
    }

    updateIngredient(index: number, newIngredient: Ingredient) {
        this.ingredients[index] = newIngredient;
        this.onAddedIngredients.next(this.ingredients);
    }

    deleteIngredient(index: number){
        this.ingredients.splice(index, 1);
        this.onAddedIngredients.next(this.ingredients);
    }
}