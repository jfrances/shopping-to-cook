export class MainService{
    activeComponent: string;

    ngOnInit(){
    }

    goTo(componentName: string){
        this.activeComponent = componentName;
    }
}