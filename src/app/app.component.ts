import { Component, OnInit } from '@angular/core';
import { MainService } from './services/main.service';
import * as firebase from 'firebase';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [MainService]
})
export class AppComponent implements OnInit {
  constructor(private mainService: MainService){}

  ngOnInit(){
    firebase.initializeApp({
      apiKey: "AIzaSyAXvzZj3PFhBJehTpBl1UORf3Pq1QlZX1M",
    authDomain: "ng-recipe-book-4dc97.firebaseapp.com"
    });
  }

  setActiveComponent(name: any){
    switch(name){
      case 'recipes': 
        this.mainService.activeComponent = 'recipes';
        break;
      case 'shopping-list': 
        this.mainService.activeComponent = 'shopping-list';
        break;
    }
  }
}
